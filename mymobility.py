#!/usr/bin/python

'Setting the position of nodes and providing mobility'

from mininet.net import Mininet
from mininet.node import Controller, OVSKernelAP
from mininet.link import TCLink
from mininet.cli import CLI
from mininet.log import setLogLevel



from mininet.net import Mininet
from mininet.topo import Topo
from mininet.node import Node, Host, RemoteController
from mininet.log import setLogLevel, info
from mininet.util import quietRun, dumpNodeConnections
from mininet.cli import CLI
from mininet.node import OVSController, Controller
from mininet.node import CPULimitedHost
from mininet.link import TCLink

#/etc/init.d/openvswitch-switch start
import timeit

clock = timeit.default_timer;

def myNet(net, sampleInterval = '.1', directory = 'ballplate'):
    h1, sta1, sta2 = net.getNodeByName('h1', 'sta1', 'sta2')
    print h1.cmd("ls")
    h1.cmd('mosquitto -p 9883 &')
    #print sta1.sendCmd()
    h = h1.sendCmd("mosquitto_sub -p 9883 -t 'test/topic' -C 1")
    #h1.cmd("mosquitto_pub -p 9883 -t 'test/topic' -m 'helloWorld'&")
    print h
    sta2.monitor()
    #h1.cmd("kill %mosquitto")
    #h1.monitor()

def runHTTP(net, sampleInterval = '.1', duration = 200, directory = 'hyttp'):
    h1, sta1, sta2 = net.getNodeByName('h1', 'sta1', 'sta2')
    import subprocess
    i = 1;
    t = directory + str(i)
    output, err = subprocess.Popen("ls", stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    while t in output:
        i += 1;
        t = directory + str(i);
    subprocess.call(['mkdir',t])
    #start tcp dump on all servers in a file with the time
    servers = [['Plant', sta1],['Controller', sta2]]
    for i in range(0, len(servers)):
        servers[i][1].cmd('tcpdump -w ' + t + '/' + servers[i][0] +  '.pcap &')
    from datetime import datetime
    dt = datetime.now()
    subprocess.call(["echo", '"' + dt.strftime('%b-%d-%H:%M.%S') +'"', ">", t +"/time.txt"])
    plantsRunning = 'cd %s; python ../nlmodel_webpy.py 9883 > plantOut.txt &'%t
    controllersRunning = 'cd %s; '%t + 'python ../nlcontroller.py pycurl bar 10.0.0.2 9883 test %s %s 6.09 3.5 -5.18 -12.08 6.58 -0.4 > controllerOut.txt &'%(duration, sampleInterval)
    
    sta1.cmd(plantsRunning)
    sta2.cmd(controllersRunning)
    T=clock()+duration
    #CLI(net)

    while clock() < T:
        output = h1.cmd("tail -n 1 %s/logging_rotatingfile_example.out"%t);
        try:
            positions= [float(i)*100 for i in output.split()[0:2]]
            positions.append(0);#print positions
            sta1.setPosition("%s,%s,%s"%tuple(positions));
        except:
            pass
        timeit.time.sleep(.02)
    sta1.cmd("kill %python")

def runROS(net, sampleInterval = '.1', duration = 40, directory = 'ryros'):
    import os
    from re import search
    h1, sta1, sta2 = net.getNodeByName('h1', 'sta1', 'sta2')
    import subprocess
    i = 1;
    t = directory + str(i)
    output, err = subprocess.Popen("ls", stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    while t in output:
        i += 1;
        t = directory + str(i);
    subprocess.call(['mkdir',t])
    #start tcp dump on all servers in a file with the time
    servers = [['Broker', h1],['Plant', sta1],['Controller', sta2]]
    rosSetupPath = "/opt/ros/lunar"  if os.path.isdir("/opt/ros/lunar") else  "/opt/ros/kinetic"
    for i in range(0, len(servers)):
        servers[i][1].cmdPrint('source %s/setup.bash'%rosSetupPath)
        servers[i][1].cmd('tcpdump -w ' + t + '/' + servers[i][0] +  '.pcap &')
        
    timeit.time.sleep(2)
    from datetime import datetime
    dt = datetime.now()
    subprocess.call(["echo", '"' + dt.strftime('%b-%d-%H:%M.%S') +'"', ">", t +"/time.txt"])
    brokerLoc = "10.0.0.1"
    brokersRunning = 'ROS_MASTER_URI=http://10.0.0.1:11311/ ROS_HOSTNAME=10.0.0.1 roscore > ' + t + '/roscoreOutput.txt &'
    plantsRunning =  'cd %s; '%t + 'ROS_MASTER_URI=http://%s:11311/ ROS_HOSTNAME=10.0.0.2 python ../nlmodel_ros.py &'%(brokerLoc)
    controllersRunning = 'cd %s; '%t + 'ROS_MASTER_URI=http://%s:11311/ ROS_HOSTNAME=10.0.0.3 python ../nlcontroller.py ros bar something somethingelse control_action__1__plant_state__1 %s %s 6.09 3.5 -5.18 -12.08 6.58 -0.4 > controllerOut.txt &'%(brokerLoc, duration, sampleInterval)
    h1.cmd(brokersRunning)
    timeit.time.sleep(5)
    sta2.cmd(controllersRunning)
    sta1.cmd(plantsRunning)
    #CLI(net)
    T=clock()+duration
    while clock() < T:
        output = h1.cmd("tail -n 1 %s/logging_rotatingfile_example.out"%t);
        try:
            positions= [float(i)*100 for i in output.split()[0:2]]
            positions.append(0);#print positions
            sta1.setPosition("%s,%s,%s"%tuple(positions));
        except:
            pass
        timeit.time.sleep(.02)
    sta1.cmd("kill %python")
    h1.cmd("kill %roscore")

def runMQTT(net, sampleInterval = '.1', duration = 10, directory = 'myqtt'):
    h1, sta1, sta2 = net.getNodeByName('h1', 'sta1', 'sta2')
    import subprocess
    i = 1;
    t = directory + str(i)
    output, err = subprocess.Popen("ls", stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    while t in output:
        i += 1;
        t = directory + str(i);
    subprocess.call(['mkdir',t])
    #start tcp dump on all servers in a file with the time
    servers = [['Broker', h1],['Plant', sta1],['Controller', sta2]]
    for i in range(0, len(servers)):
        servers[i][1].cmd('tcpdump -w ' + t + '/' + servers[i][0] +  '.pcap &')
    timeit.time.sleep(2)
    from datetime import datetime
    dt = datetime.now()
    subprocess.call(["echo", '"' + dt.strftime('%b-%d-%H:%M.%S') +'"', ">", t +"/time.txt"])
    brokerLoc = "10.0.0.1"
    brokersRunning = 'mosquitto -p 9883 > %s/mosquittoOutput.txt &'%t
    plantsRunning = 'cd %s; '%t + 'python ../nlmodel_mqtt.py %s 9883 control_action__1__plant_state__1 &'%(brokerLoc)
    controllersRunning = 'cd %s; '%t + 'python ../nlcontroller.py mqtt bar %s 9883 control_action__1__plant_state__1 %s %s 6.09 3.5 -5.18 -12.08 6.58 -0.4 > controllerOut.txt &'%(brokerLoc, duration, sampleInterval)
    h1.cmd(brokersRunning)
    timeit.time.sleep(5)
    sta2.cmd(controllersRunning)
    sta1.cmd(plantsRunning)
    T=clock()+duration
    while clock() < T:
        output = h1.cmd("tail -n 1 %s/logging_rotatingfile_example.out"%t);
        try:
            positions= [float(i)*100 for i in output.split()[0:2]]
            positions.append(0);#print positions
            sta1.setPosition("%s,%s,%s"%tuple(positions));
        except:
            pass
        timeit.time.sleep(.02)
    sta1.cmd("kill %mosquitto")


def wifiTopology():
    "Create a network."
    net = Mininet(controller=Controller, link=TCLink, accessPoint=OVSKernelAP)

    print "*** Creating nodes"
    h1 = net.addHost('h1', mac='00:00:00:00:00:01', ip='10.0.0.1/8')
    sta1 = net.addStation('sta1', mac='00:00:00:00:00:02', ip='10.0.0.2/8', position='0.0,0.0,0.0')
    sta2 = net.addStation('sta2', mac='00:00:00:00:00:03', ip='10.0.0.3/8', position='0.0,0.0,0.0')
    ap1 = net.addAccessPoint('ap1', ssid='new-ssid', mode='g', channel='1', position='0,0,0')
    ap1.setRange(300);
    c1 = net.addController('c1', controller=Controller)

    print "*** Configuring wifi nodes"
    net.configureWifiNodes()

    print "*** Associating and Creating links"
    net.addLink(ap1, h1)
    net.addLink(ap1, sta1)
    net.addLink(ap1, sta2)

    print "*** Starting network"
    net.build()
    c1.start()
    ap1.start([c1])

    "*** Available propagation models: friisPropagationLossModel, twoRayGroundPropagationLossModel, logDistancePropagationLossModel ***"
    net.propagationModel('friisPropagationLossModel', sL=2)
    '''
    """plotting graph"""
    net.plotGraph(max_x=100, max_y=100)


    '''
    net.startMobility(time=0)
    runROS(net, duration=10, directory ="rostest")
    net.stopMobility(time=5)
    print "*** Stopping network"
    net.stop()

def topology():
    "Create a network."
    net = Mininet(controller=Controller, link=TCLink, accessPoint=OVSKernelAP)
    delay = '10ms';
    jitter = '1ms';
    loss = 1;
    bw = 10;
    print "*** Creating nodes"
    h1 = net.addHost('h1', mac='00:00:00:00:00:01', ip='10.0.0.1/8')
    sta1 = net.addHost('sta1', mac='00:00:00:00:00:02', ip='10.0.0.2/8', position='0.0,0.0,0.0')
    sta2 = net.addHost('sta2', mac='00:00:00:00:00:03', ip='10.0.0.3/8', position='0.0,0.0,0.0')
    ap1 = net.addSwitch('ap1')
    c1 = net.addController('c1', controller=Controller)

    print "*** Associating and Creating links"
    net.addLink(ap1, h1, delay=delay, jitter=jitter, loss=loss, bw=bw)
    net.addLink(ap1, sta1, delay=delay, jitter=jitter, loss=loss, bw=bw)
    net.addLink(ap1, sta2, delay=delay, jitter=jitter, loss=loss, bw=bw)

    print "*** Starting network"
    net.build()
    c1.start()
    ap1.start([c1])#a switch!!
    '''
    """plotting graph"""
    net.plotGraph(max_x=100, max_y=100)
    '''
    #CLI(net)
    runROS(net, duration=6000, directory ="rostest_wired_40ms")
    #runHTTP(net)
    #runMQTT(net)
    print "*** Stopping network"
    net.stop()

if __name__ == '__main__':
    setLogLevel('info')
    topology()
