#!/usr/bin/python
import timeit

clock = timeit.default_timer;


def runROS(sampleInterval = '.1', duration = 40, directory = 'ryros'):
    import subprocess
    i = 1;
    t = directory + str(i)
    output, err = subprocess.Popen("ls", stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    while t in output:
        i += 1;
        t = directory + str(i);
    subprocess.call('mkdir %s'%t, shell=True)
    #start tcp dump on all servers in a file with the time
    rosSetupPath = "source /opt/ros/*/setup.bash; "
    subprocess.Popen("killall tcpdump", shell=True, executable="/bin/bash")
    subprocess.Popen("pkill -f 'python ../nl'", shell=True, executable="/bin/bash")
    subprocess.Popen("killall roscore", shell=True, executable="/bin/bash")
    a0 = subprocess.Popen('tcpdump -i lo -w ' + t + '/trace.pcap &', shell=True)
    timeit.time.sleep(2)
    brokerLoc = "127.0.0.1"
    brokersRunning = rosSetupPath+'ROS_MASTER_URI=http://127.0.0.1:11311/ ROS_HOSTNAME=127.0.0.1 roscore > ' + t + '/roscoreOutput.txt &'
    plantsRunning =  rosSetupPath+'cd %s; '%t + 'ROS_MASTER_URI=http://%s:11311/ ROS_HOSTNAME=127.0.0.1 python ../nlmodel_ros.py &'%(brokerLoc)
    controllersRunning = rosSetupPath+'cd %s; '%t + 'ROS_MASTER_URI=http://%s:11311/ ROS_HOSTNAME=127.0.0.1 python ../nlcontroller.py ros bar something somethingelse control_action__1__plant_state__1 %s %s 6.09 3.5 -5.18 -12.08 6.58 -0.4 > controllerOut.txt &'%(brokerLoc, duration, sampleInterval)

    a1=subprocess.Popen(brokersRunning, shell=True, executable="/bin/bash")
    timeit.time.sleep(5)
    a2=subprocess.Popen(controllersRunning, shell=True, executable="/bin/bash")
    a3=subprocess.Popen(plantsRunning, shell=True, executable="/bin/bash")
    T=clock()+duration
    while clock() < T:
        timeit.time.sleep(.02)
    a0.terminate()
    a2.terminate()
    a3.terminate()
    a1.terminate()


if __name__ == '__main__':
    runROS(duration=6000, directory ="rostest_localhost_scenario")
    #runHTTP(net)
    #runMQTT(net)
